using NUnit.Framework;
using ShopBridgeAPI.Interfaces;
using ShopBridgeAPI.Manager;
using ShopBridgeAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ShopBridgeAPI.Test
{
    public class Tests
    {
        private readonly IInventoryManager _inventoryManager;
        private readonly int _inventoryItemId;
        private readonly Inventory _inventoryToAdd;
        private readonly Inventory _inventoryToUpdate;
        public Tests(IInventoryManager inventoryManager)
        {
            _inventoryManager = inventoryManager;
            _inventoryItemId = 1;

            _inventoryToAdd.Name = "TestItem";
            _inventoryToAdd.Description = "TestDescription";
            _inventoryToAdd.Price = 1000;


            _inventoryToUpdate.ItemId = 1;
            _inventoryToUpdate.Name = "TestItem" + _inventoryToAdd.ItemId;
            _inventoryToUpdate.Description = "TestDescription" + _inventoryToAdd.Description;
            _inventoryToUpdate.Price = 1000;
        }
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task GetInventoryItemsTest()
        {
            List<Inventory> inventories = await _inventoryManager.GetInventoryItems();
        }
        [Test]
        public async Task GetInventoryItemByIdTest()
        {
            Inventory inventories = await _inventoryManager.GetInventoryItemById(_inventoryItemId);
        }
        [Test]
        public async Task AddInventoryItem()
        {
            bool success = await _inventoryManager.AddInventoryItem(_inventoryToAdd);
        }
        [Test]
        public async Task UpdateInventoryItem()
        {
            bool success= await _inventoryManager.UpdateInventoryItem(_inventoryToUpdate);
        }
        [Test]
        public async Task DeleteInventoryItem()
        {
            bool success = await _inventoryManager.DeleteInventoryItem(_inventoryItemId);
        }
    }
}